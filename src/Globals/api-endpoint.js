const API_ENDPOINT = {
  LOGIN: '/login',
  PRODUCTS_CATEGORIES: '/categories',
  ALL_PRODUCTS_CATEGORIES: (categoryId) => categoryId ? `products?category=${categoryId}` : '/products',
  PRODUCT_DETAIL: (productId) => `/products/${productId}`,
  GET_USER_PROFILE: (userName) => `/profile/${userName}`,
  CHANGE_USER_PROFILE: (userName) => `/profile/${userName}`
}

export default API_ENDPOINT
