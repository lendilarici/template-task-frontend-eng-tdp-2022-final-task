import React from 'react'
import 'assets/css/style.css'
import 'assets/css/custom.css'
import NavBar from './Component/NavBar'
import Footer from 'Component/Footer'
import ShopDetailPage from 'Pages/ShopDetailPage'
import ShopPage from 'Pages/ShopPage'
import ProfilePage from 'Pages/ProfilePage'
import LoginPage from 'Pages/LoginPage'
import HomePage from 'Pages/HomePage'
import { Navigate, Route, Routes } from 'react-router-dom'

const App = () => {
  const isLogin = window.localStorage.getItem('access_token')
  const RequiereAuth = ({ children }) => {
    return isLogin ? children : <Navigate to='/login'/>
  }

  const PublicRoute = ({ children }) => {
    return isLogin ? <Navigate to='/'/> : children
  }

  return (
        <>
            <NavBar />
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <Routes>
                    <Route path='/' element={<HomePage />}/>
                    <Route path='/login' element={
                        <PublicRoute>
                            <LoginPage />
                        </PublicRoute>
                    }/>
                    <Route path='/profile' element={
                        <RequiereAuth>
                            <ProfilePage />
                        </ RequiereAuth>
                    }
                    />

                    <Route path='/shop' element={<ShopPage />}/>
                    <Route path='/shop/:productId' element={<ShopDetailPage />}/>
                </Routes>
            </div>
            <Footer />
        </>
  )
}

export default App
