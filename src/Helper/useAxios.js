import { useState, useEffect } from 'react'
import axios from 'axios'
import CONFIG from '../Globals/config'

axios.defaults.baseURL = CONFIG.BASE_URL

const useAxios = (type, url) => {
  const [response, setResponse] = useState(null)
  const [error, setError] = useState(null)
  const [loading, setLoading] = useState(false)

  const getData = async () => {
    setLoading(true)
    try {
      const result = await axios.get(url)
      setResponse(result.data)
    } catch (err) {
      setError(err)
    } finally {
      setLoading(false)
    }
  }

  const postData = async (body) => {
    if (type === 'POST') {
      setLoading(true)
      try {
        const result = await axios.post(url, body)
        setResponse(result.data)
      } catch (err) {
        setError(err)
      } finally {
        setLoading(false)
      }
    }
  }

  const putData = async (body) => {
    if (type === 'PUT') {
      setLoading(true)
      try {
        const result = await axios.put(url, body)
        setResponse(result.data)
      } catch (err) {
        setError(err)
      } finally {
        setLoading(false)
      }
    }
  }

  useEffect(() => {
    type === 'GET' && getData()
  }, [])

  return { response, error, loading, postData, putData }
}

export default useAxios
