import Header from 'Component/Header'
import ListSubtitle from 'Component/ListSubtitle'
import Map from 'Component/Map'
import ProductCategoryCard from 'Component/ProductCategoryCard'
import API_ENDPOINT from '../Globals/api-endpoint'
import React from 'react'
import Loading from 'Component/Loading'
import useAxios from 'Helper/useAxios'

export default function HomePage () {
  const { response, loading } = useAxios('GET', API_ENDPOINT.PRODUCTS_CATEGORIES)

  return (
      <>
        <Header />
        <ListSubtitle subtitle={'Products Category'} id={'projects'} />
        {loading
          ? (
        <Loading />
            )
          : (
          <div className="row-padding">
            {response && response.data?.map((item) => <ProductCategoryCard key={item.id} item={item} />
            )}
          </div>
            )}

        <Map />
      </>
  )
}
