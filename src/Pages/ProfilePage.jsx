import ChangeProfileForm from 'Component/ChangeProfileForm'
import InputSubtitle from 'Component/InputSubtitle'
import React from 'react'

export default function ProfilePage () {
  return (
    <div className="container padding-32" id="contact" >
        <InputSubtitle subtitle={'My Profile'}/>
        <ChangeProfileForm />
    </div>
  )
}
