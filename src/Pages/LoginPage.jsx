import LoginForm from 'Component/LoginForm'
import InputSubtitle from 'Component/InputSubtitle'
import React from 'react'

export default function LoginPage () {
  return (
    <div className="container padding-32" id="contact" >
        <InputSubtitle subtitle={'Login'}/>
        <LoginForm />
    </div >
  )
}
