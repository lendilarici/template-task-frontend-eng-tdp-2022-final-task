import ListSubtitle from 'Component/ListSubtitle'
import Loading from 'Component/Loading'
import ProductCard from 'Component/ProductCard'
import API_ENDPOINT from 'Globals/api-endpoint'
import useAxios from 'Helper/useAxios'
import React, { useState, useEffect } from 'react'
import { useSearchParams } from 'react-router-dom'

export default function ShopPage () {
  const [category] = useSearchParams()
  const { response, loading } = useAxios('GET', API_ENDPOINT.ALL_PRODUCTS_CATEGORIES(category.get('category')))
  const [subTitle, setSubTitle] = useState('')

  useEffect(() => {
    category.get('category') !== null && setSubTitle(`Products Category ${response?.data.categoryDetail.name}`)
  }, [response])

  useEffect(() => {
    category.get('category') === null ? setSubTitle('All Products') : setSubTitle('Products Category')
  }, [])

  return (
    <>
      <ListSubtitle subtitle={subTitle} id={'about'}>
      </ListSubtitle>
      <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
        {loading
          ? (
          <Loading />
            )
          : (
            <>
            {response && response.data?.productList.map((item) =>
                  <ProductCard key={item.id} product={item} />
            )}
            </>
            )}
        </div>
      </>
  )
}
