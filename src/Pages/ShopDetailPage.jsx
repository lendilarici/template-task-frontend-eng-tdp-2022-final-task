import ListSubtitle from 'Component/ListSubtitle'
import ProductInformationCard from 'Component/ProductInformationCard'
import React from 'react'

export default function ShopDetailPage () {
  return (
    <>
        <ListSubtitle subtitle={'Product Information'} id={'about'}>
          <ProductInformationCard />
        </ ListSubtitle>
    </>
  )
}
