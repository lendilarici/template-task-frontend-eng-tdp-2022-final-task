const addDot = (num) => {
  if (num === null) return
  return (
    num
      .split('')
      .reverse()
      .map((digit, index) =>
        index !== 0 && index % 3 === 0 ? `${digit}.` : digit
      )
      .reverse()
      .join('')
  )
}

export { addDot }
