import React from 'react'

export default function NavBar () {
  const isLogin = window.localStorage.getItem('access_token')
  const handleLogOut = () => {
    localStorage.clear()
    window.localStorage.reload()
  }

  return (
    <div className="top" >
        <div className="bar white wide padding card">
            <a href="/" className="bar-item button"><b>EDTS</b> TDP Batch #2</a>
            <div className="right hide-small">
                <a href="/" className="bar-item button">Home</a>
                <a href="/shop" className="bar-item button">Shop</a>
                {isLogin && <a href="/profile" className="bar-item button">My Profile</a>}
                {isLogin
                  ? <a href="/" onClick={handleLogOut} className="bar-item button">Log out</a>
                  : <a href="/login" className="bar-item button">Login</a>
                }
            </div>
        </div>
    </div>
  )
}
