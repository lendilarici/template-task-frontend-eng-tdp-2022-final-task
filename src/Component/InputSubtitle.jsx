import React from 'react'

export default function InputSubtitle ({ subtitle }) {
  return (
      <>
        <h3 className="border-bottom border-light-grey padding-16">{subtitle}</h3>
        <p>Lets get in touch and talk about your next project.</p>
      </>
  )
}
