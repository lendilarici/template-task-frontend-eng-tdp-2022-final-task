import API_ENDPOINT from 'Globals/api-endpoint'
import useAxios from 'Helper/useAxios'
import React, { useState, useEffect } from 'react'

export default function LoginForm () {
  const [formData, setFormData] = useState({
    userName: '',
    password: ''
  })

  const handleChange = (e) => {
    setFormData(prevState => ({ ...prevState, [e.target.name]: e.target.value }))
  }

  const { response, postData, error, loading } = useAxios('POST', API_ENDPOINT.LOGIN)

  useEffect(() => {
    error && alert(error.response.data.message)
  }, [error])

  useEffect(() => {
    response && handleLogin()
  }, [response])

  const handleSubmit = (e) => {
    e.preventDefault()
    const loginData = {
      username: formData.userName,
      password: formData.password
    }
    postData(loginData)
  }

  const handleLogin = () => {
    localStorage.setItem('access_token', JSON.stringify(response.data.access_token))
    localStorage.setItem('user_info', JSON.stringify({ username: formData.userName }))
    window.location.replace('/')
  }

  return (
    <form onSubmit={handleSubmit}>
        <input onChange={handleChange} className="input border" type="text" placeholder="Username" required name="userName" />
        <input onChange={handleChange} className="input section border" type="password" placeholder="Password" required name="password" />
        <button disabled={loading} className="button black section" type="submit">
            <i className="fa fa-paper-plane" />
            {loading ? <>Loading</> : <>Login</>}
        </button>
    </form>
  )
}
