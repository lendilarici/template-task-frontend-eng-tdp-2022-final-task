import API_ENDPOINT from 'Globals/api-endpoint'
import useAxios from 'Helper/useAxios'
import React, { useEffect, useState } from 'react'

export default function ChangeProfileForm () {
  const userName = window.localStorage.getItem('user_info')
  const { response: putResponse, putData, loading } = useAxios('PUT', API_ENDPOINT.CHANGE_USER_PROFILE(JSON.parse(userName).username))
  const { response: getResponse } = useAxios('GET', API_ENDPOINT.GET_USER_PROFILE(JSON.parse(userName).username))

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    phoneNumber: '',
    password: '',
    retypePassword: ''
  })

  useEffect(() => {
    getResponse && setFormData(prevState => ({
      ...prevState,
      name: getResponse.data.name,
      email: getResponse.data.email,
      phoneNumber: getResponse.data.phoneNumber
    }))
  }, [getResponse])

  const [errorMessage, setErrorMessage] = useState({
    nameError: '',
    emailError: '',
    phoneNumberError: '',
    passwordError: '',
    retypePasswordError: ''
  })

  const [allErrorStatus, setAllErrorStatus] = useState({
    nameError: false,
    emailError: false,
    phoneNumberError: false,
    passwordError: false,
    retypePasswordError: false
  })

  const validation = () => {
    let errorStatus = false

    if (formData.name === '') {
      errorStatus = true
      setErrorMessage(prevState => ({ ...prevState, nameError: 'Name is required' }))
      setAllErrorStatus(prevState => ({ ...prevState, nameError: true }))
    } else {
      setAllErrorStatus(prevState => ({ ...prevState, nameError: false }))
    }
    if (formData.email === '') {
      setErrorMessage(prevState => ({ ...prevState, emailError: 'Email is required' }))
      setAllErrorStatus(prevState => ({ ...prevState, emailError: true }))

      errorStatus = true
    } else if (!/^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/.test(formData.email)) {
      errorStatus = true
      setErrorMessage(prevState => ({ ...prevState, emailError: 'Email is invalid' }))
      setAllErrorStatus(prevState => ({ ...prevState, emailError: true }))
    } else {
      setAllErrorStatus(prevState => ({ ...prevState, emailError: false }))
    }
    if (formData.phoneNumber === '') {
      setErrorMessage(prevState => ({ ...prevState, phoneNumberError: 'Phone number is required' }))
      setAllErrorStatus(prevState => ({ ...prevState, phoneNumberError: true }))

      errorStatus = true
    } else if (isNaN(formData.phoneNumber) || formData.phoneNumber.length !== 12) {
      setErrorMessage(prevState => ({ ...prevState, phoneNumberError: 'Phone number invalid' }))
      setAllErrorStatus(prevState => ({ ...prevState, phoneNumberError: true }))

      errorStatus = true
    } else {
      setAllErrorStatus(prevState => ({ ...prevState, phoneNumberError: false }))
    }
    if (formData.password === '') {
      setErrorMessage(prevState => ({ ...prevState, passwordError: 'Password is required' }))
      setAllErrorStatus(prevState => ({ ...prevState, passwordError: true }))

      errorStatus = true
    } else {
      setAllErrorStatus(prevState => ({ ...prevState, passwordError: false }))
    }
    if (formData.retypePassword === '') {
      setErrorMessage(prevState => ({ ...prevState, retypePasswordError: 'Retype password is required' }))
      setAllErrorStatus(prevState => ({ ...prevState, retypePasswordError: true }))

      errorStatus = true
    } else if (formData.retypePassword !== formData.password) {
      setErrorMessage(prevState => ({ ...prevState, retypePasswordError: 'Retype password invalid' }))
      setAllErrorStatus(prevState => ({ ...prevState, retypePasswordError: true }))

      errorStatus = true
    } else {
      setAllErrorStatus(prevState => ({ ...prevState, retypePasswordError: false }))
    }

    return errorStatus
  }

  const handleChange = (e) => {
    setFormData(prevState => ({ ...prevState, [e.target.name]: e.target.value }))
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const changeProfileData = {
      name: formData.name,
      email: formData.email,
      phoneNumber: formData.phoneNumber,
      password: formData.password
    }
    if (!validation()) {
      putData(changeProfileData)
    }
  }

  useEffect(() => {
    putResponse && window.confirm(putResponse.message).then(
      window.location.reload()
    )
  }, [putResponse])

  return (
    <form onSubmit={handleSubmit}>
        <input onChange={handleChange} className="input section border" type="text" placeholder="Name" name="name" value={formData.name} />
        {allErrorStatus.nameError && <div style={{ color: '#EF144A' }}>{errorMessage.nameError}</div>}
        <input onChange={handleChange} className="input section border" type="text" placeholder="Email" name="email" value={formData.email} />
        {allErrorStatus.emailError && <div style={{ color: '#EF144A' }}>{errorMessage.emailError}</div>}
        <input onChange={handleChange} className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12} value={formData.phoneNumber}/>
        {allErrorStatus.phoneNumberError && <div style={{ color: '#EF144A' }}>{errorMessage.phoneNumberError}</div>}
        <input onChange={handleChange} className="input section border" type="password" placeholder="Password" name="password" value={formData.password}/>
        {allErrorStatus.passwordError && <div style={{ color: '#EF144A' }}>{errorMessage.passwordError}</div>}
        <input onChange={handleChange} className="input section border" type="password" placeholder="Retype Password" name="retypePassword" value={formData.retypePassword}/>
        {allErrorStatus.retypePasswordError && <div style={{ color: '#EF144A' }}>{errorMessage.retypePasswordError}</div>}
        <button disabled={loading} className="button black section" type="submit">
            <i className="fa fa-paper-plane" /> {loading ? <>Loading</> : <>Update</>}
        </button>
    </form>
  )
}
