import React from 'react'
import { addDot } from 'Utils/utils'

export default function ProductCard ({ product }) {
  return (
    <div className="product-card">
        {product.discount > 0 && <div className="badge">Discount</div>}
        <div className="product-tumb">
            <img src={product.image} alt={product.title} />
        </div>
        <div className="product-details">
            <span className="product-catagory">{product.category}</span>
            <h4>
                <a href={`/shop/${product.id}`}>{product.title}</a>
            </h4>
            <p>{product.description}</p>
            <div className="product-bottom-details">
                <div className="product-price">Rp {addDot(product.price)}</div>
                <div className="product-links">
                    <a href={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></a>
                </div>
            </div>
        </div>
    </div>
  )
}
