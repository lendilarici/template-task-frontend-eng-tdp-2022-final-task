import React from 'react'

export default function ListSubtitle ({ subtitle, id, children }) {
  return (
    <div className="container padding-32" id={id}>
        <h3 className="border-bottom border-light-grey padding-16">{subtitle}</h3>
        {children}
    </div>
  )
}
