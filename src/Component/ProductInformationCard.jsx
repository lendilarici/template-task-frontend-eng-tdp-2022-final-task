import API_ENDPOINT from 'Globals/api-endpoint'
import useAxios from 'Helper/useAxios'
import React, { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { addDot } from 'Utils/utils'
import Loading from './Loading'

export default function ProductInformationCard () {
  const { productId } = useParams()
  const navigate = useNavigate()
  const { response, loading } = useAxios('GET', API_ENDPOINT.PRODUCT_DETAIL(productId))
  const [finalDiscountPrice, setFinalDiscountPrice] = useState('')
  const [finalPrice, setFinalPrice] = useState('')
  const [inputNumber, setInputNumber] = useState({
    total: 0
  })
  const [addToChartStatus, setAddToChartStatus] = useState(false)
  const isLogin = window.localStorage.getItem('access_token')

  useEffect(() => {
    calculateDiscountPrice(response?.data.price, response?.data.discount)
    calculatePrice(response?.data.price)

    if (inputNumber.total === 0 || response?.data.stock === 0) {
      setAddToChartStatus(true)
    } else {
      setAddToChartStatus(false)
    }
    console.log(response?.data.stock === 0)
  }, [response, inputNumber])

  const calculateDiscountPrice = (price, discount) => {
    const result = (price - price * (discount / 100)) * inputNumber.total
    setFinalDiscountPrice(result.toString())
  }

  const calculatePrice = (price) => {
    const result = price * inputNumber.total
    setFinalPrice(result.toString())
  }

  const handleChange = (e) => {
    setInputNumber(prevState => ({ ...prevState, [e.target.name]: parseInt(e.target.value) }))
  }

  const handleClick = () => {
    if (isLogin) {
      alert('Product added to cart successfully')
      navigate('/')
    } else {
      navigate('/login')
    }
  }
  return (
    <>
      {loading
        ? (
        <Loading />
          )
        : (
          <>
          {response && (
            <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                  <div className="col l3 m6 margin-bottom">
                      <div className="product-tumb">
                          <img src={response.data.image} alt={`Product ${response.data.id}`} />
                      </div>
                  </div>
                  <div className="col m6 margin-bottom">
                      <h3>{response.data.title}</h3>
                      <div style={{ marginBottom: '32px' }}>
                          <span>Category : <strong>{response.data.category}</strong></span>
                          <span style={{ marginLeft: '30px' }}>Review : <strong>{response.data.rate}</strong></span>
                          <span style={{ marginLeft: '30px' }}>Stock : <strong>{response.data.stock}</strong></span>
                          <span style={{ marginLeft: '30px' }}>Discount : <strong>{`${response.data.discount} %`}</strong></span>
                      </div>
                      <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                          Rp {addDot(response.data.price)}
                      </div>
                      <div style={{ marginBottom: '32px' }}>
                          {response.data.description}
                      </div>
                      <div style={{ marginBottom: '32px' }}>
                          <div><strong>Quantity : </strong></div>
                          <input onChange={handleChange} type="number" className="input section border" name="total" min={0} max={response.data.stock} value={inputNumber.total}/>
                      </div>
                      <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                          Sub Total : Rp {addDot(finalDiscountPrice)}
                          {response.data.discount > 0 && <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. {addDot(finalPrice)}</strong></span>}
                      </div>
                      {response.data.stock === 0
                        ? (
                          <button className='button light-grey block' disabled={true}>Empty Stock</button>
                          )
                        : (
                          <button onClick={handleClick} disabled={addToChartStatus} className='button light-grey block' >Add to cart</button>
                          )}
                  </div>
              </div>
          )}
          </>
          )}
    </>
  )
}
